package com.buutcamp.springdemo.filereadwrite.main;

import com.buutcamp.springdemo.filereadwrite.filehandles.FileHandle;

public class RunApp {

    public RunApp() {

        ApplicationContext appCtx = new AnnotationConfigApplicationCOntext(BeanConfig.class);
        FileHandle fileHandle = (FileHandle) appCtx.getBean("fileHandle");

        fileHandle.writeLine("text.txt","anotherTesti");
        fileHandle.readLine("text.txt",2);
        fileHandle.clearFile("text.txt");
        
        ((AnnotationConfigApplicationContext) appCtx).close();
    }
}
