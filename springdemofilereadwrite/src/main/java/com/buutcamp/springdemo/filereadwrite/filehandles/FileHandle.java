package com.buutcamp.springdemo.filereadwrite.filehandles;

import java.io.*;

public class FileHandle {

    public void writeLine(String filePath, String lineToWrite) {
        File file = new File(filePath);
        try {
            if (file.canWrite()) {
                try {
                    PrintWriter printWriter = new PrintWriter(new BufferedWriter(new FileWriter(file,true)));
                    printWriter.println(lineToWrite);
                    printWriter.close();
                } catch (FileNotFoundException  e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void readLine(String filePath, int lineNumber) {
        File file = new File(filePath);
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

            //Loop over lines to get to the specified lineNumber
            //Works fine for small files
            for (int i = 0; i < lineNumber; i++) {
                bufferedReader.readLine();
            }
            System.out.println(bufferedReader.readLine());
            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void clearFile(String filePath) {
        File file = new File(filePath);
        try {
            FileWriter fw = new FileWriter(file);
            fw.close();
        } catch (IOException e){
            e.printStackTrace();
        }
    }
}
