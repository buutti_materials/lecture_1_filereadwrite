package com.buutcamp.springdemo.filereadwrite.config;

import com.buutcamp.springdemo.filereadwrite.filehandles.FileHandle;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class BeanConfig {
    @Bean(name="fileHandle")
    @Scope("singleton")
    public FileHandle getFileHandle() {
        return new FileHandle();
    }
}
